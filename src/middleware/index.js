const { default: App } = require('@azulejo/client/lib/App');

const { renderReact } = require('../utils/render-component');

// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  // Add your custom middleware here. Remember that
  // in Express, the order matters.
  const render = renderReact();

  app.use((req, res) => {
    res.render('index',  {
      chunks: {
        index: render(App, {})[0]
      }
    });
  });
};
