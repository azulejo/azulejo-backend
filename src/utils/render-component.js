const React = require('react');
const { renderToString } = require('react-dom/server');

// eslint-disable-next-line no-unused-vars
exports.renderReact = function(toString = renderToString, styles = {}){
  return (Node, props = {}) => {

    return [toString(React.createElement(Node, props))];
  };
};
