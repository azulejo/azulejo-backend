# @azulejo/backend

> Application server with SSR support

## About

Is used to serve applications similar to `@azulejo/client`.

## Modes

There are two possible modes:
1. Static bundle mode
2. SSR compatible mode

### Static mode

`public` setting allows to load precompiled static assets.
```json
// config/default.json
{
  "host": "localhost",
  "port": 3030,
  "public": "../node_modules/@azulejo/client/build",
}
```

### SSR mode
`assets` setting suppres `public` path and allows to define directory with custom assets.
```json
// config/default.json
{
  "host": "localhost",
  "port": 3030,
  "public": "../node_modules/@azulejo/client/build",
  "assets": "../node_modules/@azulejo/client/out",
}
```
In that case PUG template([`index.pug`](./src/templates/index.pug)) will be served.

## Engine

### About

This project uses [Feathers](http://feathersjs.com). An open source web framework for building modern real-time applications.

### Getting Started

Getting up and running is as easy as 1, 2, 3.

1. Make sure you have [NodeJS](https://nodejs.org/) and [npm](https://www.npmjs.com/) installed.
2. Install your dependencies

    ```
    cd path/to/azulejo-backend; npm install
    ```

3. Start your app

    ```
    npm start
    ```

### Testing

Simply run `npm test` and all your tests in the `test/` directory will be run.

### Scaffolding

Feathers has a powerful command line interface. Here are a few things it can do:

```
$ npm install -g @feathersjs/cli          # Install Feathers CLI

$ feathers generate service               # Generate a new Service
$ feathers generate hook                  # Generate a new Hook
$ feathers generate model                 # Generate a new Model
$ feathers help                           # Show all commands
```

### Help

For more information on all the things you can do with Feathers visit [docs.feathersjs.com](http://docs.feathersjs.com).

## License

Copyright (c) 2016

Licensed under the [MIT license](LICENSE).
