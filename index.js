module.exports = (({ NODE_ENV }) => NODE_ENV === 'production' ? require('./lib') : require('./src'))(process.env);
